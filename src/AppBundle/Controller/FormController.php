<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Contact;

class FormController extends Controller
{
    private $contact;
    private $user;
    private $address;
    private $city;
    private $province;
    private $address_digital;
    private $phone;
    private $em;

    /**
     * @Route("/{page}/forms/user/show/{id}", 
     *          requirements = {"id" : "\d+"},
     *          name="userprofile")
     */
    public function showProfileAction(int $page, int $id)
    {

        $this->contact = $this->getProfileData($id);
        $provinces = $this->em->getRepository('AppBundle:Province')->findAll();
        $cities = $this->em->getRepository('AppBundle:City')->findAll();
        $addresses = $this->em->getRepository('AppBundle:Address')->findAll();

        return $this->render('profile/profile.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'profile' => $this->contact,
            'page' => $page,
            'provinces' => $provinces,
            'cities' => $cities,
            'addresses' => $addresses,
        ]);
        
    }

    private function getProfileData($id) : Contact {

        $this->em = $this->getDoctrine()->getManager();

        return $this->em->getRepository('AppBundle:Contact')->getUserProfile($id);

    }
    /**
     * here data is and array of Contact entity
    */
    private function setProfileData(int $id) {

        $this->contact = $this->getProfileData($id);
        $this->user = $this->contact->getFkuser();
        $this->address = $this->contact->getFkaddress();
        $this->address_digital = $this->contact->getFkaddressDigital();
        $this->phone = $this->address_digital->getFkphone();
        $this->city = $this->address->getFkcity();
        $this->province = $this->city->getFkprovince();

    }

    /**
     * @Route("/{page}/forms/user/edit/{id}", 
     *          requirements = {"id" : "\d+"},
     *          name="editprofile")
     */
    public function editProfileAction(Request $request, int $page, int $id)
    {

        if ($request->getMethod() == Request::METHOD_POST) {

            $this->setProfileData($id);
            $this->updateProfile($request);

        }

        return $this->redirectToRoute('homepage', array('page' => $page), 301);

    }

    private function updateProfile(Request $request) {
        
        $name = $request->request->get('name');
        if (0 != strcmp($name, $this->user->getName())) {
            $this->user->setName($name);
        }

        $surname = $request->request->get('surname');
        if (0 != strcmp($surname, $this->user->getSurname())) {
            $this->user->setSurname($surname);
        }

        $this->em->merge($this->user);
        $this->em->flush();

        $street = $request->request->get('street');
        if (0 != strcmp($street, $this->address->getStreet())) {
            $this->address->setStreet($street);
        }

        $idCity = $request->request->get('city');
        if ($idCity != $this->city->getIdcity()) {
            $this->address->setFkcity($this->em->getRepository('AppBundle:City')->find($idCity));
        }

        $this->em->merge($this->address);
        $this->em->flush();

        $idProvince = $request->request->get('province');
        if ($idProvince != $this->province->getIdprovince()) {
            $this->city->setFkprovince($this->em->getRepository('AppBundle:Province')->find($idProvince));
        }

        $this->em->merge($this->city);
        $this->em->flush();

    }

    /**
     * @Route("/{page}/forms/user/delete/{id}", 
     *          requirements = {"id" : "\d+"},
     *          name="userdeleteprofile")
     */
    public function deleteProfileAction(int $id, int $page) {

        $this->setProfileData($id);

        $this->em->remove($this->contact);
        $this->em->remove($this->user);
        $this->em->remove($this->address);
        $this->em->remove($this->address_digital);
        $this->em->remove($this->phone);
        $this->em->flush();

        return $this->redirectToRoute('homepage', array('page' => $page), 301);

    }

}
