<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    const TABLE_ROWS = 12;
    /**
     * @Route("/{page}", 
     *          defaults = {"page" = "1"},
     *          requirements = { "page" : "\d+"},
     *          name="homepage")
     */
    public function indexAction($page)
    {

        $data = $this->getDoctrine()->getManager()->getRepository('AppBundle:Contact')->getTableData();
        
        $paginator = $this->get('knp_paginator');
        $data = $paginator->paginate(
                                        $data,
                                        $page,
                                        self::TABLE_ROWS
                                    );

        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
            'data' => $data,
            'rows' => self::TABLE_ROWS,
            'page' => $page - 1,
        ]);
    }
}
