<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * City
 *
 * @ORM\Table(name="city", uniqueConstraints={@ORM\UniqueConstraint(name="idcity_UNIQUE", columns={"idcity"}), @ORM\UniqueConstraint(name="zipcode_UNIQUE", columns={"zipcode"})}, indexes={@ORM\Index(name="fk_city_province_idx", columns={"fk_idprovince"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CityRepository")
 */
class City
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="zipcode", type="string", length=6, nullable=false)
     */
    private $zipcode;

    /**
     * @var integer
     *
     * @ORM\Column(name="idcity", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcity;

    /**
     * @var \AppBundle\Entity\Province
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Province", inversedBy="cities")
     * @ORM\JoinColumn(name="fk_idprovince", referencedColumnName="idprovince", nullable=false)
     */
    private $fkprovince;

    /**
     * One City has Many Address.
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Address", mappedBy="fkcity", cascade={"merge", "persist"}, orphanRemoval=true)
     */
    private $addresses;
    
        public function __construct() {
            $this->addresses = new ArrayCollection();
        }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return City
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set zipcode
     *
     * @param string $zipcode
     *
     * @return City
     */
    public function setZipcode($zipcode)
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    /**
     * Get zipcode
     *
     * @return string
     */
    public function getZipcode()
    {
        return $this->zipcode;
    }

    /**
     * Get idcity
     *
     * @return integer
     */
    public function getIdcity()
    {
        return $this->idcity;
    }

    /**
     * Set fkprovince
     *
     * @param \AppBundle\Entity\Province $fkprovince
     *
     * @return City
     */
    public function setFkprovince(\AppBundle\Entity\Province $fkprovince = null)
    {
        $this->fkprovince = $fkprovince;

        return $this;
    }

    /**
     * Get fkprovince
     *
     * @return \AppBundle\Entity\Province
     */
    public function getFkprovince()
    {
        return $this->fkprovince;
    }
}
