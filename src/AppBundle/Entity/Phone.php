<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Phone
 *
 * @ORM\Table(name="phone", uniqueConstraints={@ORM\UniqueConstraint(name="idphone_UNIQUE", columns={"idphone"}), @ORM\UniqueConstraint(name="number_UNIQUE", columns={"number"})})
 * @ORM\Entity
 */
class Phone
{
    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", length=32, nullable=false)
     */
    private $number;

    /**
     * @var integer
     *
     * @ORM\Column(name="idphone", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idphone;

    /**
     * One Phone has Many AddressDigital.
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\AddressDigital", mappedBy="fkphone", cascade={"merge", "persist"}, orphanRemoval=true)
     */
    private $digitalAddresses;
    
        public function __construct() {
            $this->digitalAddresses = new ArrayCollection();
        }

    /**
     * Set number
     *
     * @param string $number
     *
     * @return Phone
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Get idphone
     *
     * @return integer
     */
    public function getIdphone()
    {
        return $this->idphone;
    }
}
