<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * AddressDigital
 *
 * @ORM\Table(name="address_digital", uniqueConstraints={@ORM\UniqueConstraint(name="idaddres_digital_UNIQUE", columns={"idaddress_digital"}), @ORM\UniqueConstraint(name="email_UNIQUE", columns={"email"})}, indexes={@ORM\Index(name="fk_address_digital_phone_idx", columns={"fk_idphone"})})
 * @ORM\Entity
 */
class AddressDigital
{
    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=64, nullable=true)
     */
    private $email;

    /**
     * @var integer
     *
     * @ORM\Column(name="idaddress_digital", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idaddressDigital;

    /**
     * @var \AppBundle\Entity\Phone
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Phone", inversedBy="digitalAddresses", cascade={"remove"})
     * @ORM\JoinColumn(name="fk_idphone", referencedColumnName="idphone", nullable=false)
     */
    private $fkphone;

    /**
     * One Addresses Digital has Many Contact.
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Contact", mappedBy="fkaddressDigital", cascade={"merge", "persist"}, orphanRemoval=true)
     */
    private $contacts;
    
    public function __construct() {
        $this->contacts = new ArrayCollection();
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return AddressDigital
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Get idaddressDigital
     *
     * @return integer
     */
    public function getIdaddressDigital()
    {
        return $this->idaddressDigital;
    }

    /**
     * Set fkphone
     *
     * @param \AppBundle\Entity\Phone $fkphone
     *
     * @return AddressDigital
     */
    public function setFkphone(\AppBundle\Entity\Phone $fkphone = null)
    {
        $this->fkphone = $fkphone;

        return $this;
    }

    /**
     * Get fkphone
     *
     * @return \AppBundle\Entity\Phone
     */
    public function getFkphone()
    {
        return $this->fkphone;
    }
}
