<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Address
 *
 * @ORM\Table(name="address", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"idaddress"})}, indexes={@ORM\Index(name="fk_address_city_idx", columns={"fk_idcity"})})
 * @ORM\Entity
 */
class Address
{
    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string", length=64, nullable=true)
     */
    private $street;

    /**
     * @var integer
     *
     * @ORM\Column(name="idaddress", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idaddress;

    /**
     * One Addresses has Many Contacts.
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Contact", mappedBy="fkaddress", cascade={"merge", "persist"}, orphanRemoval=true)
     */
    private $contacts;

    public function __construct() {
        $this->contacts = new ArrayCollection();
    }

    /**
     * @var \AppBundle\Entity\City
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\City", inversedBy="addresses")
     * @ORM\JoinColumn(name="fk_idcity", referencedColumnName="idcity", nullable=false)
     */
    private $fkcity;



    /**
     * Set street
     *
     * @param string $street
     *
     * @return Address
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Get idaddress
     *
     * @return integer
     */
    public function getIdaddress()
    {
        return $this->idaddress;
    }

    /**
     * Set fkcity
     *
     * @param \AppBundle\Entity\City $fkcity
     *
     * @return Address
     */
    public function setFkcity(\AppBundle\Entity\City $fkcity = null)
    {
        $this->fkcity = $fkcity;

        return $this;
    }

    /**
     * Get fkcity
     *
     * @return \AppBundle\Entity\City
     */
    public function getFkcity()
    {
        return $this->fkcity;
    }
}
