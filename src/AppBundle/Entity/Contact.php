<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Contact
 *
 * @ORM\Table(name="contact", uniqueConstraints={@ORM\UniqueConstraint(name="idcontact_UNIQUE", columns={"idcontact"})}, indexes={@ORM\Index(name="fk_contact_address_digital_idx", columns={"fk_idaddress_digital"}), @ORM\Index(name="fk_contact_address_idx", columns={"fk_idaddress"}), @ORM\Index(name="fk_contact_User_idx", columns={"fk_iduser"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ContactRepository")
 */
class Contact
{
    /**
     * @var boolean
     *
     * @ORM\Column(name="current", type="boolean", nullable=false)
     */
    private $current = '1';

    /**
     * @var integer
     *
     * @ORM\Column(name="idcontact", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idcontact;

    /**
     * @var \AppBundle\Entity\Address
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Address", inversedBy="contacts", cascade={"remove"})
     * @ORM\JoinColumn(name="fk_idaddress", referencedColumnName="idaddress", nullable=false)
     */
    private $fkaddress;

    /**
     * @var \AppBundle\Entity\AddressDigital
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\AddressDigital", inversedBy="contacts", cascade={"remove"})
     * @ORM\JoinColumn(name="fk_idaddress_digital", referencedColumnName="idaddress_digital")
     */
    private $fkaddressDigital;

    /**
     * @var \AppBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="contacts", cascade={"remove"})
     * @ORM\JoinColumn(name="fk_iduser", referencedColumnName="iduser")
     */
    private $fkuser;



    /**
     * Set current
     *
     * @param boolean $current
     *
     * @return Contact
     */
    public function setCurrent($current)
    {
        $this->current = $current;

        return $this;
    }

    /**
     * Get current
     *
     * @return boolean
     */
    public function getCurrent()
    {
        return $this->current;
    }

    /**
     * Get idcontact
     *
     * @return integer
     */
    public function getIdcontact()
    {
        return $this->idcontact;
    }

    /**
     * Set fkaddress
     *
     * @param \AppBundle\Entity\Address $fkaddress
     *
     * @return Contact
     */
    public function setFkaddress(\AppBundle\Entity\Address $fkaddress = null)
    {
        $this->fkaddress = $fkaddress;

        return $this;
    }

    /**
     * Get fkaddress
     *
     * @return \AppBundle\Entity\Address
     */
    public function getFkaddress()
    {
        return $this->fkaddress;
    }

    /**
     * Set fkaddressDigital
     *
     * @param \AppBundle\Entity\AddressDigital $fkaddressDigital
     *
     * @return Contact
     */
    public function setFkaddressDigital(\AppBundle\Entity\AddressDigital $fkaddressDigital = null)
    {
        $this->fkaddressDigital = $fkaddressDigital;

        return $this;
    }

    /**
     * Get fkaddressDigital
     *
     * @return \AppBundle\Entity\AddressDigital
     */
    public function getFkaddressDigital()
    {
        return $this->fkaddressDigital;
    }

    /**
     * Set fkuser
     *
     * @param \AppBundle\Entity\User $fkuser
     *
     * @return Contact
     */
    public function setFkuser(\AppBundle\Entity\User $fkuser = null)
    {
        $this->fkuser = $fkuser;

        return $this;
    }

    /**
     * Get fkuser
     *
     * @return \AppBundle\Entity\User
     */
    public function getFkuser()
    {
        return $this->fkuser;
    }
}
