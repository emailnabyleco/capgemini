<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Province
 *
 * @ORM\Table(name="province", uniqueConstraints={@ORM\UniqueConstraint(name="idprovince_UNIQUE", columns={"idprovince"}), @ORM\UniqueConstraint(name="name_UNIQUE", columns={"name"})})
 * @ORM\Entity
 */
class Province
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=64, nullable=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="idprovince", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idprovince;

    /**
     * One Province has Many Cities.
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\City", mappedBy="fkprovince")
     */
    private $cities;
    
        public function __construct() {
            $this->cities = new ArrayCollection();
        }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Province
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get idprovince
     *
     * @return integer
     */
    public function getIdprovince()
    {
        return $this->idprovince;
    }
}
