<?php
namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ContactRepository extends EntityRepository
{
    public function getTableData() {

        $qb = $this->createQueryBuilder('c');
        $qb->select(
                    'c', 'user',
                    'address', 'address_digital', 
                    'phone', 'city', 'province');
        $qb->leftJoin('c.fkuser', 'user');
        $qb->leftJoin('c.fkaddress', 'address');
        $qb->leftJoin('address.fkcity', 'city');
        $qb->leftJoin('city.fkprovince', 'province');
        $qb->leftJoin('c.fkaddressDigital', 'address_digital');
        $qb->leftJoin('address_digital.fkphone', 'phone');

        $qb->addOrderBy('user.surname', 'ASC');
        $qb->addOrderBy('user.name', 'ASC');

        return $qb->getQuery()->getResult();

    }

    public function getUserProfile(int $userId) {

        $profileData = null;
        
        $qb = $this->createQueryBuilder('c');
        $qb->select(
                    'c', 'user', 'address', 'address_digital', 
                    'phone', 'city', 'province'
                    );
        
        $qb->leftJoin('c.fkuser', 'user');
        $qb->leftJoin('c.fkaddress', 'address');
        $qb->leftJoin('address.fkcity', 'city');
        $qb->leftJoin('city.fkprovince', 'province');
        $qb->leftJoin('c.fkaddressDigital', 'address_digital');
        $qb->leftJoin('address_digital.fkphone', 'phone');
        $qb->where('c.fkuser = :iduser');
        $qb->setParameter('iduser', $userId);

        try {

            $profileData = $qb->getQuery()->getSingleResult();

        } catch(NonUniqueResultException $ex) {
            
            echo 'Problem with Query in ContactRepository->getUserProfile : ',  $ex->getMessage(), "\n";

        }

        return $profileData;

    }

}

?>