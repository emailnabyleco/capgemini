<?php
namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class CityRepository extends EntityRepository
{
    public function findAll() {

        $qb = $this->createQueryBuilder('c');
        $qb->select('c');
        $qb->orderBy('c.name', 'ASC');

        return $qb->getQuery()->getResult();

    }

}

?>