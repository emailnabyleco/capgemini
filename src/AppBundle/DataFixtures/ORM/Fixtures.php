<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use AppBundle\Entity\Province;
use AppBundle\Entity\City;
use AppBundle\Entity\Address;
use AppBundle\Entity\AddressDigital;
use AppBundle\Entity\Phone;
use AppBundle\Entity\Contact;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class Fixtures extends Fixture
{
    /**
     * how many records to generate
    */
    const RECORDS = 32;

    /**
     * @var Doctrine\Common\Persistence\ObjectManager
    */
    private $manager;

    public function load(ObjectManager $manager)
    {

        $this->manager = $manager;

        $this->genProvinces();
        $this->genCities();
        $this->genAdress();
        $this->genPhone();
        $this->genPersonalData();

    }

    private function genPersonalData() {

        $phones = $this->manager->getRepository('AppBundle:Phone')->findAll();
        $phonesCount = count($phones) - 1;

        $names = [
                    'Jan', 'Filip', 'Franciszek', 'Mikolaj', 'Aleksander', 'Kacper', 'Wojciech',
                    'Maja', 'Hanna', 'Zofia', 'Amelia', 'Alicja', 'Aleksandra', 'Natalia'
                    ];

        $surnames = [
                        'Nowak', 'Kowalski', 'Wisniewski', 'Wojcik', 'Kowalczyk', 'Kaminski', 'Lewandowski', 
                        'Dabrowski', 'Zielinski', 'Szymanski', 'Wozniak', 'Kozlowski', 'Jankowski', 'Mazur',
                        'Wojciechowski', 'Kwiatkowski', 'Krawczyk', 'Piotrowski'
                        ];

        for ($i = 0; $i < self::RECORDS; $i++) {

            $user = new User;
            $user->setName($names[mt_rand(0, count($names) - 1)]);
            $user->setSurname($surnames[mt_rand(0, count($surnames) - 1)]);

            $this->manager->persist($user);
            $this->manager->flush();

            $newDigitalAddress = new AddressDigital;
            $newDigitalAddress->setEmail(strtolower($user->getSurname() . "." . $user->getName()) . mt_rand(1, 1000) . "@gmail.com");
            $newDigitalAddress->setFkphone($phones[$i]);//$phones[mt_rand(0, $phonesCount)]);

            $this->manager->persist($newDigitalAddress);
            $this->manager->flush();

            $addresses = $this->manager->getRepository('AppBundle:Address')->findAll();
            $addressesCount = count($addresses) - 1;

            $contact = new Contact;
            $contact->setFkuser($user);
            $contact->setFkaddress($addresses[$i]);//$addresses[mt_rand(0, $addressesCount)]);
            $contact->setFkaddressDigital($newDigitalAddress);
            $contact->setCurrent(true);

            $this->manager->persist($contact);
            $this->manager->flush();

        }

    }

    private function genAdress()
    {
        $cities = $this->manager->getRepository('AppBundle:City')->findAll();
        $citiesCount = count($cities) - 1;

        $streets = [
                    'Polna', 'Leśna', 'Słoneczna', 'Krótka', 
                    'Szkolna', 'Ogrodowa','Lipowa','Brzozowa',
                    'Łąkowa', 'Kwiatowa'
                    ];

        $streetsCount = count($streets) - 1;
        
        for ($i = 0; $i < self::RECORDS; $i++) {
            
            $city = $cities[mt_rand(0, $citiesCount)];
            $addres = new Address;
            $addres->setFkcity($city);
            $no = mt_rand(1, 101);
            $addres->setStreet($streets[mt_rand(0, $streetsCount)] . " " . $no);

            $this->manager->persist($addres);

        }

        $this->manager->flush();

    }

    private function genPhone() {

        
        for ($i = 0; $i < self::RECORDS; $i++) {

            $phone = new Phone;
            $phoneNumber = "";

            for($j = 0; $j < 9; $j++) {

                $phoneNumber .= mt_rand(0, 9);

            }

            $phone->setNumber($phoneNumber);
            $this->manager->persist($phone);

        }

        $this->manager->flush();

    }

    private function genCities() {

        $provinces = $this->manager->getRepository('AppBundle:Province')->findAll();

        $cities = [
                    ['Gliwice' => '11-111'], ['Cracow' => '11-222'], ['Poznan' => '11-333'], 
                    ['Lublin' => '11-444'], ['Wroclaw' => '11-555'], ['Opole' => '11-666'], 
                    ['Katowice' => '11-777'], ['Warsaw' => '11-888'], ['Gdansk' => '11-999'],
                    ['Olsztyn' => '12-111'], ['Rzeszow' => '12-222'], ['Bialystok' => '13-333'],
                    ['Wieliczka' => '14-444'], ['Zakopane' => '44-444'], ['Wisla' => '15-131'], 
                    ['Lodz' => '53-113'], ['Zamosc' => '21-243'], ['Zielona Gora' => '10-114']
        ];

        $citiesCount = count($cities);
        for ($i = 0; $i < $citiesCount; $i++) {

            $province = $provinces[mt_rand(0, count($provinces) - 1)];

            $city = new City;
            $city->setName(key($cities[$i]));
            $city->setZipcode($cities[$i][key($cities[$i])]);
            $city->setFkprovince($province);

            $this->manager->persist($city);

        }

        $this->manager->flush();

    }

    /**
     * method for creating provinces in db
     * 
     * @return AppBundle\Entity\Province
    */
    private function genProvinces()
    {
        $provinces = [
            'The Voivodeship of Lower Silesia',
            'The Voivodeship of Cuiavia & Pomerania',
            'The Voivodeship of Lublin',
            'The Voivodeship of Lubusz',
            'The Voivodeship of Lodz',
            'The Voivodeship of Lesser Poland',
            'The Voivodeship of Mazovia',
            'The Voivodeship of Opole',
            'The Voivodeship of Sub-Carpathia',
            'The Voivodeship of Podlassia',
            'The Voivodeship of Pomerania',
            'The Voivodeship of Silesia',
            'The Voivodeship of Kielce',
            'The Voivodeship of Varmia and Masuria',
            'The Voivodeship of Greater Poland',
            'The Voivodeship of Western Pomerania'
            ];

        $provincesCount = count($provinces);
        for ($i = 0; $i < $provincesCount; $i++) {

            $province = new Province;
            $province->setName($provinces[$i]);
            $this->manager->persist($province);

        }

        $this->manager->flush();

    }

}

?>