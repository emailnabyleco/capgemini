Capgemini Task
========================

Please implement standard CRUD operation for User entity, which will be responsible for keeping the following data:

- Name

- Surname

- Telephone Number

- Address


Feel free to choose any PHP technology and at the same time try not to resort to any auto-generated solutions as it would make it difficult for us to asses you coding skills. If you have questions or concerns related to the task above - please resolve it as you think it should be. As the result, please send me a zipped solution. Additionally please attach simple instruction how to run the solution. Please use popular file sharing platforms Dropbox, Google Drive or One Drive. Due to safety reasons we are unable to download the source code from unknown hosting platform. Additionally please attach a simple instruction on how to run your solution.

This exercise aims at showing your best coding practices.

Ps. We would be more than happy if you could share your public source code repository (github, visualstudioonlie, etc)
